local imports = {
    'config',
    'modules',
    'packer_config'
}

for _, import in ipairs(imports) do
    local ok, import = pcall(require, import)
    if not ok then
	vim.notify("couldn't require a module : " .. import)
    end
end

