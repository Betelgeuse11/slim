local M = {}

M.setup = function()
	vim.api.nvim_create_user_command("Buffers", M._buffers, {})
end

M._buffers = function()
	local buffers = vim.api.nvim_list_bufs()

	local select_items = {}
	for _, buffer in ipairs(buffers) do
		if vim.api.nvim_buf_is_loaded(buffer) then
			table.insert(select_items, buffer)
		end
	end

	vim.ui.select(select_items, {
		prompt = 'Buffers : ',
		format_item = M._format_item,
	}, M._on_confirm)
end

M._format_item = function(buffer)
	local buf_path = vim.api.nvim_buf_get_name(buffer)
	local filename = string.gsub(buf_path, '.*/', '')
	return filename
end

M._on_confirm = function(buffer)
	if not buffer then
		return
	end
	vim.api.nvim_set_current_buf(buffer)
end

return M
