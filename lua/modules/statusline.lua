local M = {
	fmt = string.format,
	git = {
		current_branch = 'git branch --show-current 2>/dev/null',
		max_branchname_size = 10,
		state = {
			last = 0,
		}
	},
	-- custom highlights, used in config.highlights
	highlights = { -- Inactive/Default statusline appearance
		'hi default StatuslineDefault	ctermfg=0 ctermbg=236',
		-- Mode related hightlights
		'hi default link StatuslineModeNormal	Substitute',
		'hi default link StatuslineModeInsert	DiffAdd',
		'hi default link StatuslineModeVisual	Visual',
		'hi default link StatuslineModeReplace	Search',
		'hi default link StatuslineModeDefault	DiffText',
		-- Git, LSP and file related highlights
		'hi default StatuslineFile ctermfg=15 ctermbg=237',
		'hi default StatuslineIcon ctermfg=15 ctermbg=239',
		'hi default	StatuslineBuffer ctermfg=0 ctermbg=13',
		'hi default StatuslineGitBranch ctermfg=15 ctermbg=239',
		'hi default	StatuslineDiagErr ctermfg=0 ctermbg=9',
		'hi default	StatuslineDiagWrn ctermfg=0 ctermbg=11',
	}

}

M.setup_statusline = function()
	_G.Statusline = M

	M.__generate_git_state()

	vim.api.nvim_exec(
		[[augroup Statusline
			au!
			au WinEnter,BufEnter * setlocal statusline=%!v:lua.Statusline.active_statusline()
		augroup END]],
		false
	)

	vim.api.nvim_create_autocmd('FocusGained', {
		pattern = '*',
		callback = M.__generate_git_state,
	})
end

M.active_statusline = function()
	local statusline = ''

	for _, section in ipairs(M.sections) do
		local result = section
		if type(result) == 'function' then
			result = result()
		end

		statusline = statusline .. result
	end

	return statusline
end


M._mode_section = function()
	local mode = vim.fn.mode():upper()

	return M.modes[mode]
end

M._file_icon_section = function()
	local filetype = vim.bo.filetype
	if filetype == '' then
		filetype = vim.fn.expand('%:e')
	end
	local icon = M.icons[filetype]

	return '%#StatuslineIcon# ' .. icon .. ' %#StatuslineDefault#'
end

M._filename_section = function()
	return '%#StatuslineFile# %t %#StatuslineDefault#'
end

M._diagnostics_section = function()
	local diag_counts = {}
	local levels = {
		['error'] = vim.diagnostic.severity.ERROR,
		['warn'] = vim.diagnostic.severity.WARN,
	}

	for key, value in pairs(levels) do
		diag_counts[key] = #vim.diagnostic.get(0, { severity = value })
	end

	return string.format(
		"%%#StatuslineDiagErr# E%s %%#StatuslineDiagWrn# W%s %%#StatuslineDefault#",
		diag_counts['error'],
		diag_counts['warn']
	)
end

-- Could use some refacto
M.__generate_git_state = function()
	-- Only allow one refresh per sencond.
	if os.time() - M.git.state.last < 1 then
		return
	end

	local branch_prc = io.popen(M.git.current_branch)
	if not branch_prc then
		return
	end

	local branchname = branch_prc:read()
	branch_prc:close()

	M.git.state = {
		last = os.time(),
		branchname = branchname or "none",
	}
end

M._git_branch_section = function()
	local branchname = M.git.state.branchname
	if branchname and #branchname > M.git.max_branchname_size then
		branchname = string.sub(branchname, 1, M.git.max_branchname_size) .. '...'
	end
	return M.fmt('%%#StatuslineGitBranch#  %s %%#StatuslineDefault#', branchname)
end

M._buffer_info = function()
	return '%#StatuslineBuffer# B%n %{&modified?"":""} %#StatuslineDefault#'
end

M.icons = setmetatable({
	['go'] = '',
	['lua'] = '',
	['dockerfile'] = '',
	['ruby'] = '',
	['cr'] = '',
	['c'] = '',
	['cpp'] = 'ﭱ',
}, {
	__index = function()
		return ''
	end
})

-- code snippet yanked from https://github.com/echasnovski/mini.nvim/blob/main/lua/mini/statusline.lua
local CTRL_V = vim.api.nvim_replace_termcodes('<C-V>', true, true, true)

M.modes = setmetatable({
	['N'] = '%#StatuslineModeNormal#  %#Cursor# N %#StatuslineDefault#',
	['I'] = '%#StatuslineModeInsert# פֿ %#Cursor# I %#StatuslineDefault#',
	['V'] = '%#StatuslineModeVisual#  %#Cursor# V %#StatuslineDefault#',
	['^V'] = '%#StatuslineModeVisual#   %#Cursor# V %#StatuslineDefault#',
	[CTRL_V] = '%#StatuslineModeVisual#  %#Cursor# V %#StatuslineDefault#',
	['R'] = '%#StatuslineModeReplace# 屢%#Cursor# R %#StatuslineDefault#',
	['C'] = '%#StatuslineModeDefault#  %#Cursor# C %#StatuslineDefault#',
}, {
	__index = function()
		return '%#StatuslineModeDefault#  %#Cursor# U %#StatuslineDefault#'
	end
})

M.sections = {
	M._mode_section,
	M._git_branch_section,
	M._filename_section,
	'%=',
	M._diagnostics_section,
	M._buffer_info,
	M._file_icon_section,
}

return M
