return {
	{
		'neovim/nvim-lspconfig',
		config = function()
			local servers = require('config.lsp_servers')
			local ok, lsp = pcall(require, 'lspconfig')
			if not ok then
				vim.notify [[lspconfig couldn't be loaded]]
				return nil
			end

			local on_attach = function(_, bufnr)
				-- Setting up omnifunc to completion's one
				vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.MiniCompletion.completefunc_lsp')
				-- Setup lsp keymaps online for this buffer
				require('config.keymaps').setup_lsp_keymaps(bufnr)
			end

			for _, server in pairs(servers) do
				local is_table = type(server) == 'table'

				local name = is_table and server[1] or server
				local config = { on_attach = on_attach }
				if is_table then
					config = vim.tbl_extend('force', config, server[2])
				end

				lsp[name].setup(config)
			end
		end,
	}
}
