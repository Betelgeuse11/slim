local lok, lsp = pcall(require, 'plugins.lsp')
if not lok then
	vim.notify[[lsp config could not be load]]
	return
end

local uok, users = pcall(require, 'plugins.user')
if not uok then
	vim.notify[[users plugins could not be load]]
	return
end

-- concat tables
for i = 1, #lsp do
	users[#users + 1] = lsp[i]
end

return users
