return {
	'wbthomason/packer.nvim',
	-- 'franbach/miramare',
	'sainnhe/everforest',
	-- 'rebelot/kanagawa.nvim',
	-- 'RRethy/nvim-base16',
	-- 'cranberry-clockworks/coal.nvim',
	-- 'catppuccin/nvim',

	{
		'rhysd/git-messenger.vim',
		config = function()
			vim.g.git_messenger_floating_win_opts = { border = 'single' }
		end
	},

	{
		'Betelgeuse1/zealua.nvim',
		config = function()
			require('zealua').setup {
				auto_keymaps = true,
			}
		end
	},
	{
		'https://gitlab.com/Betelgeuse11/align.nvim',
		config = function()
			require('align').setup {}
		end
	},

	{
		'nvim-treesitter/nvim-treesitter',
		branch = 'v0.8.0',
		run = function() require('nvim-treesitter.install').update({ with_sync = true }) end,
		config = function()
			require('nvim-treesitter.configs').setup {
				ensure_installed = { "lua", "python", "go" },
				highlight = {
					enable = true,
					additional_vim_regex_highlighting = false,
				},
			}
		end
	},

	{
		'echasnovski/mini.nvim',
		config = function()
			local mini_plugins = {
				'mini.ai',
				'mini.comment',
				'mini.cursorword',
				'mini.indentscope',
				'mini.pairs',
				'mini.surround',

				{
					'mini.completion',
					config = {
						lsp_completion = {
							-- `source_func` should be one of 'completefunc' or 'omnifunc'.
							source_func = 'omnifunc',
							-- Not auto-setuping
							auto_setup = false,
						},
					},
				},
				{
					'mini.jump2d',
					config = {
						mappings = {
							start_jumping = '<leader>j',
						},
					},
				},
			}
			-- Load each mini_plugins
			for _, mini in ipairs(mini_plugins) do
				-- if mini is a table, use the first key as the plugin name
				-- otherwise mini is just a string so use its value directly.
				local config = {}
				local name = mini
				if type(mini) == 'table' then
					config = mini.config
					name = mini[1]
				end
				-- could either have a specify config, like for mini.completion,
				-- or none in which case, we just use defaults.
				require(name).setup(config or {})
			end
		end,
	},
}
