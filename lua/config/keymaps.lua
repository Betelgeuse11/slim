--====================================================
--==========             Module             ==========
--====================================================
local M = {}

M.keymap = vim.api.nvim_set_keymap
M.default_opts = { noremap = true, silent = true }

-- buffer keymaps custom function
M.buf_keymap = function(lft, rgt, buffer)
	local opts = M.default_opts
	opts['buffer'] = buffer or 0
	vim.keymap.set('n', lft, rgt, opts)
end
-- Default setup function, it's called at startup time
M.setup = function()
	-- Changing leader key
	M.keymap("", "<Space>", "<Nop>", M.default_opts)
	vim.g.mapleader = " "
	vim.g.maplocalleader = " "

	-- NOH
	M.keymap('n', '<leader>h', ':noh<CR>', M.default_opts)

	-- Yank
	M.keymap('n', '<leader>y', '"+yy', M.default_opts)
	M.keymap('v', '<leader>y', '"+y', M.default_opts)
	M.keymap('n', '<leader>p', '"+p', M.default_opts)
	M.keymap('v', '<leader>p', '"+p', M.default_opts)

	-- Buffers mappings
	M.keymap('n', '<leader>bh', ':bp<cr>', M.default_opts)
	M.keymap('n', '<leader>bl', ':bn<cr>', M.default_opts)
	M.keymap('n', '<leader>bb', ':Buffers<cr>', { noremap = true })
end

M.lsp_keymaps = function(bufnr)
	local leader_map = '<leader>l'
	local lsp_mappings = {
		{ 'D', vim.lsp.buf.declaration },
		{ 'd', vim.lsp.buf.definition },
		{ 'h', vim.lsp.buf.hover },
		{ 's', vim.lsp.buf.document_symbol },
		{ 'a', vim.lsp.buf.signature_help },
		{ 'i', vim.lsp.buf.implementation },

		{ 't', vim.lsp.buf.type_definition },
		{ 'n', vim.lsp.buf.rename },
		{ 'c', vim.lsp.buf.code_action },
		{ 'r', vim.lsp.buf.references },
		{ 'f', function() vim.lsp.buf.format{async=true} end },
	}

	M.set_group_keymaps(leader_map, lsp_mappings, bufnr)
end

M.diagnostic_keymaps = function(bufnr)
	local leader_map = '<leader>d'
	local diagnostic_mappings = {
		{ 'h', vim.diagnostic.goto_prev },
		{ 'l', vim.diagnostic.goto_next },
		{ 's', vim.diagnostic.open_float },
	}

	M.set_group_keymaps(leader_map, diagnostic_mappings, bufnr)
end

M.setup_lsp_keymaps = function(bufnr)
	M.lsp_keymaps(bufnr)
	M.diagnostic_keymaps(bufnr)
end

M.set_group_keymaps = function(group_identifier, mappings, bufnr)
	bufnr = bufnr or 0
	for _, mapping in ipairs(mappings) do
		M.buf_keymap(group_identifier .. mapping[1], mapping[2], bufnr)
	end
end

return M
