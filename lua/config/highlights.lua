local M = {}

M.highlights = {
	'hi default link MiniJump2dSpot OrangeSign'
}

M.setup_hightlights = function()
	for _, hi in ipairs(M.highlights) do
		vim.cmd(hi)
	end

	local status_hightlights = require('modules.statusline').highlights
	for _, hi in ipairs(status_hightlights) do
		vim.cmd(hi)
	end
end

return M
