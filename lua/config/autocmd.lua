-- TODO Add an autocmd to reload git stats on VimResume
-- Hightlight on yank
vim.api.nvim_create_autocmd(
	{ "TextYankPost" },
	{
		callback = function()
			vim.highlight.on_yank{higroup="Yellow", timeout=250}
		end
	}
)

local ok, highlights = pcall(require, 'config.highlights')
if not ok then
    vim.notify[[couldn't load highlights config file. skipping autocmd.]]
    return
end
-- Change highlight when a colorscheme is loaded
vim.api.nvim_create_autocmd(
	{ "ColorScheme" },
	{
		callback = function()
			highlights.setup_hightlights()
		end
	}
)

-- Setting svelte with html highlight + svelte filetype so
-- lsp know how to handle it.
vim.api.nvim_create_autocmd(
	{ "BufReadPost" },
	{
		pattern = { "*.svelte" },
		command = "syntax on | set filetype=svelte syntax=html "
	}
)

-- open folds on BufEnter
vim.api.nvim_create_autocmd(
	{ "BufReadPost", "FileReadPost" },
	{
		pattern = "*",
		command = "normal zR",
	}
)

-- Close QF window on selection
vim.api.nvim_create_autocmd(
	{ "FileType" },
	{
		pattern = "qf",
		command = "nnoremap <buffer> <CR> <CR>:cclose<CR>"
	}
)
