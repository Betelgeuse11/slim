require('config.autocmd')
require('config.highlights')

require('config.keymaps').setup()

vim.api.nvim_command [[colorscheme everforest]]

vim.o.number = true
vim.o.relativenumber = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.signcolumn = 'yes:1'
vim.o.laststatus = 3 -- global statusline instead of per-window one
vim.o.termguicolors = true

vim.o.foldmethod = 'indent'
vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
