local ok, plugins = pcall(require, 'plugins')
if not ok then
	vim.notify[[Couldn't require plugins]]
	return
end

return require('packer').startup({
    function(use)
	for _, plugin in ipairs(plugins) do
		use(plugin)
	end
    end,
    config = {
	display = {
	    open_fn = require('packer.util').float,
	}
    }
})
