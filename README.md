# slim

## description
minimal and lightweight neovim configuration using mini.nvim as the main plugin source.

## todo
### priority
- [ ] https://github.com/neovim/nvim-lspconfig/wiki/UI-Customization
- [ ] all git related things in modules/statusline should go in modules/git
- [ ] create a module/keymap for under cursor search 
- [ ] custom statusline since I want one completely different from mini.statusline
  - [x] mode  
  - [x] file  
  - [x] buffer  
  - [x] line count  
  - [x] git  
    - [x] branch
    - [x] nums of pull/push
    - [ ] reload git information with an autocmd on FocusGained + cooldown
    - [ ] verify if the cwd is a git one before doing all those shenanigans

### optional
- statusline
    - [ ] create a small website for easy highlights customization
    - [ ] might add a non-short option for mode\_section
    - [ ] git blame

